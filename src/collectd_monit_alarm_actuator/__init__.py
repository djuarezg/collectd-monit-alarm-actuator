import collectd
import subprocess
import yaml
import os

NAMESPACE_FIELDS = ['plugin', 'plugin_instance', 'type', 'type_instance', 'DataSource']
CONFIG = {
    "alarms_directory": "/etc/collectd.d/alarms"
}

class ActuatorNotFoundException(Exception):
    pass

def configure_callback(conf):
    global CONFIG
    for node in conf.children:
        key = node.key.lower()
        if key == 'alarmsdirectory':
            CONFIG['alarms_directory'] = node.values[0]
    collectd.register_notification(notification_callback)


def notification_callback(notification):
    namespace = _generate_namespace(notification)
    if notification.severity == collectd.NOTIF_FAILURE:
        _find_and_run_actuator(namespace, notification.alert_name)
        

def _generate_namespace(notification):
    """ Generates the namespace of a notification based on the
        configured namespace_fields in the global CONFIG dictionary
    Args:
        notification: Collectd notification object
    Returns:
        List: List containing all the namespace values in the represented order
    """
    namespace = []
    for field in NAMESPACE_FIELDS:
        try:
            if getattr(notification, field) is not None:
                namespace.append(getattr(notification, field))
        except AttributeError:
            if field in notification.meta:
                namespace.append(notification.meta[field])
    return list(filter(None, namespace))


def _load_actuator(namespace, alert_name):
    """ Loads the actuators dictionary configured in the system
    Args:
        namespace (array): alarm namespace sent by the notification event
        alert_name: alert name of the alarm just committed
    Returns:
        string: Containing the actuator command for the specified alarm_id
    Exceptions:
        ActuatorNotFoundException: Raises the exception when config file or actuator option can't be found
    """
    if alert_name:
        file_dir = CONFIG['alarms_directory'] + '/' + alert_name + '.yaml'
    if alert_name and os.path.isfile(file_dir):
        try:
            with open(file_dir) as alarms_yaml_file:
                alarm_config = yaml.load(alarms_yaml_file)
                if 'actuator' in alarm_config:
                    return alarm_config['actuator']
                else:
                    collectd.debug("Configuration for " + alert_name + " found, no actuator defined")
        except (OSError, IOError):
            pass
    else:
        namespace_names = ["_".join(namespace[0:x]) for x in reversed(range(1, len(namespace) + 1))]
        for name in namespace_names:
            try:
                file_dir = CONFIG['alarms_directory'] + '/' + name + '.yaml'
                with open(file_dir) as alarms_yaml_file:
                    alarm_config = yaml.load(alarms_yaml_file)
                    if 'actuator' in alarm_config:
                        return alarm_config['actuator']
                    else:
                        collectd.debug("Configuration for " + name + " found, no actuator defined")
            except (OSError, IOError):
                pass
    raise ActuatorNotFoundException


def _find_and_run_actuator(namespace, alert_name):
    """ Sends and alarm document and checks the result
    Args:
        namespace (array): alarm namespace sent by the notification event
        alert_name: alert name of the alarm just committed
    """
    try:
        command = _load_actuator(namespace, alert_name)
        collectd.info("Running actuator for alarm '{0}': {1}".format("_".join(namespace), command))
        subprocess.call(command, shell=True)
    except (OSError) as e:
        collectd.error("An error occurred while running actuator for '{0}': {1}".format("_".join(namespace), e))
    except ActuatorNotFoundException:
        return

collectd.register_config(configure_callback)
