Actuator Collectd Plugin
============================

Configuration
-------------

Example::

    <Plugin "python">

      Import "collectd_monit_alarm_actuator"

      <Module "collectd_monit_alarm_actuator">
          AlarmsDirectory "/etc/collectd.d/alarms"
      </Module>

    </Plugin>


* ``AlarmsDirectory``: (Optional) Path where the MONIT alarm namespace definitions are stored in YAML files. Default: "/etc/collectd.d/alarms".

Only one actuator will run per threshold alarm. The actuator defined in the most specific alarm name will override all the others.
(e.g. 'df_root_percent_bytes_used' ovrerrides 'df')

Check the README from collectd-monit-alarm-handler for more information about alarm and actuator definitions:
https://gitlab.cern.ch/monitoring/collectd-monit-alarm-handler