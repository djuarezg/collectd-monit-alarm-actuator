import sys
import os
import pytest
from mock import MagicMock, Mock, PropertyMock, patch

NOTIF_FAILURE = 4
ACTUATORS_DIR = './data'

@pytest.fixture
def collectd_actuator():
    collectd = MagicMock(NOTIF_FAILURE = 4)
    with patch.dict('sys.modules', {'collectd': collectd}):
        import collectd_monit_alarm_actuator
        yield collectd_monit_alarm_actuator

def test_configure_no_file_provided(collectd_actuator):
    collectd_actuator.configure_callback(Mock(children = []))
    assert collectd_actuator.CONFIG['alarms_directory'] == "/etc/collectd.d/alarms"

def test_configure_valid(collectd_actuator):
    collectd_actuator.configure_callback(Mock(children = [Mock(key = 'AlarmsDirectory', values = [ACTUATORS_DIR])]))
    assert collectd_actuator.CONFIG['alarms_directory'] == ACTUATORS_DIR

def test_notification_with_actuator(collectd_actuator):
    collectd_actuator.configure_callback(Mock(children = [Mock(key = 'AlarmsDirectory', values = [ACTUATORS_DIR])]))
    collectd_actuator.notification_callback(Mock(plugin='df', plugin_instance='root', type='percent', type_instance='bytes_used', DataSource='', severity=collectd_actuator.collectd.NOTIF_FAILURE, alert_name=None))
    assert os.path.exists('./test.file')
    os.remove('./test.file')

def test_notification_with_actuator_and_alert_name(collectd_actuator):
    collectd_actuator.configure_callback(Mock(children = [Mock(key = 'AlarmsDirectory', values = [ACTUATORS_DIR])]))
    collectd_actuator.notification_callback(Mock(plugin='df', plugin_instance='root', type='percent', type_instance='bytes_used', DataSource='', severity=collectd_actuator.collectd.NOTIF_FAILURE, alert_name='alarm_name'))
    assert os.path.exists('./test.file')
    os.remove('./test.file')

def test_load_actuator_from_wrong_file(collectd_actuator):
    with pytest.raises(collectd_actuator.ActuatorNotFoundException):
        collectd_actuator.configure_callback(Mock(children = [Mock(key = 'AlarmsDirectory', values = [ACTUATORS_DIR])]))
        collectd_actuator._load_actuator('stuff.yaml', None)

def test_load_actuator_with_alarm_name(collectd_actuator):
    collectd_actuator.configure_callback(Mock(children = [Mock(key = 'AlarmsDirectory', values = [ACTUATORS_DIR])]))
    collectd_actuator._load_actuator('other-stuff.yaml', 'alarm_name')

def test_configured_alarm_without_actuator(collectd_actuator):
    with pytest.raises(collectd_actuator.ActuatorNotFoundException):
        collectd_actuator.configure_callback(Mock(children = [Mock(key = 'AlarmsDirectory', values = [ACTUATORS_DIR])]))
        collectd_actuator._load_actuator('alarm_without_actuator.yaml', None)

def test_notification_without_actuator(collectd_actuator):
    collectd_actuator.configure_callback(Mock(children = [Mock(key = 'AlarmsDirectory', values = [ACTUATORS_DIR])]))
    collectd_actuator.notification_callback(Mock(plugin='alarm', plugin_instance='without', type='actuator', type_instance='file', DataSource='', severity=collectd_actuator.collectd.NOTIF_FAILURE, alert_name=None))

def test_notification_without_config(collectd_actuator):
    collectd_actuator.configure_callback(Mock(children = [Mock(key = 'AlarmsDirectory', values = [ACTUATORS_DIR])]))
    collectd_actuator.notification_callback(Mock(plugin='tst', plugin_instance='my', type='random', type_instance='stuff', DataSource='', severity=collectd_actuator.collectd.NOTIF_FAILURE, alert_name=None))
