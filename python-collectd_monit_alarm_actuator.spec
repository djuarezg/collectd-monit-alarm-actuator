# Created by pyp2rpm-3.3.2
%global pypi_name collectd_monit_alarm_actuator

Name:           python-%{pypi_name}
Version:        1.1.0
Release:        1%{?dist}
Summary:        Collectd Plugin to run commands based on Collectd notifications

License:        Apache II
URL:            https://gitlab.cern.ch/monitoring/collectd-monit-alarm-actuator
Source0:        https://gitlab.cern.ch/monitoring/collectd-monit-alarm-actuator/-/archive/%{version}/collectd-monit-alarm-actuator-%{version}.tar.gz
BuildArch:      noarch

%if 0%{?el6}
%global py_version python2
%define __python /usr/bin/python2
BuildRequires:  python2-devel
BuildRequires:  python-setuptools
%else
%if 0%{?el7}
%global py_version python2
%define __python /usr/bin/python2
BuildRequires:  python2-devel
BuildRequires:  python-setuptools
%else
%if 0%{?el8}
%global py_version python3
%define __python /usr/bin/python3
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
%endif
%endif
%endif

%description

%package -n %{py_version}-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide %{py_version}-%{pypi_name}}

%description -n %{py_version}-%{pypi_name}

%prep
%autosetup -n collectd-monit-alarm-actuator-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%clean
%{__rm} -rf %{buildroot}

%post -n %{py_version}-%{pypi_name}
/sbin/service collectd condrestart >/dev/null 2>&1 || :

%postun -n %{py_version}-%{pypi_name}
if [ $1 -eq 0 ]; then
    /sbin/service collectd condrestart >/dev/null 2>&1 || :
fi

%files -n %{py_version}-%{pypi_name}
%doc README.rst NEWS.txt LICENSE
%{python_sitelib}/%{pypi_name}
%{python_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%changelog
* Tue Feb 18 2020 Simone Brundu <simone.brundu@cern.ch> - 1.1.0-1
- [MONIT-2389] Adapted package for Python 3 and Centos 8

* Thu Jul 11 2019 Simone Brundu <simone.brundu@cern.ch> - 1.0.5-1
- [MONIT-2104] Make the alarm actuator compatible with the alert name and not only namespace

* Fri Sep 21 2018 Borja Garrido <borja.garrido.bear@cern.ch> - 1.0.4-1
- Fixes for Python 2.6 and namespace generation

* Wed Sep 12 2018 Asier Aguado <asier.aguado@cern.ch> - 1.0.3-1
- Allow actuators from all the handler namespace.

* Mon Sep 10 2018 Asier Aguado <asier.aguado@cern.ch> - 1.0.2-1
- Move actuator configuration to alarms directory.

* Wed Aug 29 2018 Asier Aguado <asier.aguado@cern.ch> - 1.0.1-1
- Use YAML to define actuators.

* Mon Aug 27 2018 Asier Aguado <asier.aguado@cern.ch> - 1.0.0-1
- Initial package.
